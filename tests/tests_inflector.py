#!/usr/bin/env python
# -*- coding: utf-8 -*-

from testtools import TestCase
from libindic.VibhakthiGenerator import Malayalam as MLVibhakthiGenerator
import collections


class MalayalamStemmerTest(TestCase):

    def setUp(self):
        super(MalayalamStemmerTest, self).setUp()
        self.vibhakthi_generator = MLVibhakthiGenerator()

    def test_accusative(self):
        '''
        Accusative - പ്രതിഗ്രാഹിക
        '''
        words = collections.OrderedDict({u'അവനെ': u'അവൻ',
                                         u'അവളെ': u'അവൾ',
                                         u'രമയെ': u'രമ',
                                         u'കഴിവിനെ': u'കഴിവ്',
                                         u'ആപത്തിനെ': u'ആപത്ത്',
                                         u'വാകയെ': u'വാക',
                                         u'അനിലിനെ': u'അനിൽ',
                                         u'അവരെ': u'അവർ',
                                         u'ചെമ്മണ്ണിനെ': u'ചെമ്മണ്ണ്',
                                         u'വാസുകിയെ': u'വാസുകി',
                                         u'വാസ്കോയെ': u'വാസ്കോ',
                                         u'വാത്മീകത്തിനെ': u'വാത്മീകം',
                                         u'ചക്കിനെ': u'ചക്ക്',
                                         u'അച്ചിനെ': u'അച്ച്',
                                         u'പട്ടിനെ': u'പട്ട്',
                                         u'കത്തിനെ': u'കത്ത്',
                                         u'തേപ്പിനെ': u'തേപ്പ്',
                                         u'പയ്യിനെ': u'പയ്യ്',
                                         u'കല്ലിനെ': u'കല്ല്',
                                         u'രാമുവിനെ': u'രാമു',
                                         u'കാടിനെ': u'കാട്'
                                         })
        for expected, word in words.items():
            obtained = self.vibhakthi_generator.generate(word, 2)
            assert obtained == expected

    def test_conjuctive(self):
        '''
        Conjuctive - സംയോജിക
        '''
        words = collections.OrderedDict({u'അവനോട്': u'അവൻ',
                                         u'അവളോട്': u'അവൾ',
                                         u'രമയോട്': u'രമ',
                                         u'കഴിവിനോട്': u'കഴിവ്',
                                         u'ആപത്തിനോട്': u'ആപത്ത്',
                                         u'വാകയോട്': u'വാക',
                                         u'അനിലിനോട്': u'അനിൽ',
                                         u'അവരോട്': u'അവർ',
                                         u'ചെമ്മണ്ണിനോട്': u'ചെമ്മണ്ണ്',
                                         u'വാസുകിയോട്': u'വാസുകി',
                                         u'വാസ്കോയോട്': u'വാസ്കോ',
                                         u'വാത്മീകത്തിനോട്': u'വാത്മീകം',
                                         u'ചക്കിനോട്': u'ചക്ക്',
                                         u'അച്ചിനോട്': u'അച്ച്',
                                         u'പട്ടിനോട്': u'പട്ട്',
                                         u'കത്തിനോട്': u'കത്ത്',
                                         u'തേപ്പിനോട്': u'തേപ്പ്',
                                         u'പയ്യിനോട്': u'പയ്യ്',
                                         u'കല്ലിനോട്': u'കല്ല്',
                                         u'രാമുവിനോട്': u'രാമു',
                                         u'കാടിനോട്': u'കാട്'
                                         })
        for expected, word in words.items():
            obtained = self.vibhakthi_generator.generate(word, 3)
            assert obtained == expected

    def test_dative(self):
        '''
        Dative - ഉദ്ദേശിക
        '''
        words = collections.OrderedDict({u'അവന്': u'അവൻ',
                                         u'അവൾക്ക്': u'അവൾ',
                                         u'രമയ്ക്ക്': u'രമ',
                                         u'കഴിവിന്': u'കഴിവ്',
                                         u'ആപത്തിന്': u'ആപത്ത്',
                                         u'വാകയ്ക്ക്': u'വാക',
                                         u'അനിലിന്': u'അനിൽ',
                                         u'അവർക്ക്': u'അവർ',
                                         u'ചെമ്മണ്ണിന്': u'ചെമ്മണ്ണ്',
                                         u'വാസുകിയ്ക്ക്': u'വാസുകി',
                                         u'വാസ്കോയ്ക്ക്': u'വാസ്കോ',
                                         u'വാത്മീകത്തിന്': u'വാത്മീകം',
                                         u'ചക്കിന്': u'ചക്ക്',
                                         u'അച്ചിന്': u'അച്ച്',
                                         u'പട്ടിന്': u'പട്ട്',
                                         u'കത്തിന്': u'കത്ത്',
                                         u'തേപ്പിന്': u'തേപ്പ്',
                                         u'പയ്യിന്': u'പയ്യ്',
                                         u'കല്ലിന്': u'കല്ല്',
                                         u'രാമുവിന്': u'രാമു',
                                         u'കാടിന്': u'കാട്'
                                         })
        for expected, word in words.items():
            obtained = self.vibhakthi_generator.generate(word, 4)
            assert obtained == expected

    def test_instrumental(self):
        '''
        Instrumental - പ്രയോജിക
        '''
        words = collections.OrderedDict({u'അവനാൽ': u'അവൻ',
                                         u'അവളാൽ': u'അവൾ',
                                         u'രമയാൽ': u'രമ',
                                         u'കഴിവിനാൽ': u'കഴിവ്',
                                         u'ആപത്തിനാൽ': u'ആപത്ത്',
                                         u'വാകയാൽ': u'വാക',
                                         u'അനിലിനാൽ': u'അനിൽ',
                                         u'അവരാൽ': u'അവർ',
                                         u'ചെമ്മണ്ണിനാൽ': u'ചെമ്മണ്ണ്',
                                         u'വാത്മീകത്തിനാൽ': u'വാത്മീകം',
                                         u'ചക്കിനാൽ': u'ചക്ക്',
                                         u'അച്ചിനാൽ': u'അച്ച്',
                                         u'പട്ടിനാൽ': u'പട്ട്',
                                         u'കത്തിനാൽ': u'കത്ത്',
                                         u'പയ്യിനാൽ': u'പയ്യ്',
                                         u'രാമുവിനാൽ': u'രാമു',
                                         u'കാടിനാൽ': u'കാട്'
                                         })
        for expected, word in words.items():
            obtained = self.vibhakthi_generator.generate(word, 5)
            assert obtained == expected

    def test_possessive(self):
        '''
        Possessive - സംബന്ധിക
        '''
        words = collections.OrderedDict({u'അവന്റെ': u'അവൻ',
                                         u'അവന്റെ': u'അവൻ',
                                         u'അവളുടെ': u'അവൾ',
                                         u'രമയുടെ': u'രമ',
                                         u'കഴിവിന്റെ': u'കഴിവ്',
                                         u'ആപത്തിന്റെ': u'ആപത്ത്',
                                         u'വാകയുടെ': u'വാക',
                                         u'അനിലിന്റെ': u'അനിൽ',
                                         u'അവരുടെ': u'അവർ',
                                         u'ചെമ്മണ്ണിന്റെ': u'ചെമ്മണ്ണ്',
                                         u'വാത്മീകത്തിന്റെ': u'വാത്മീകം',
                                         u'ചക്കിന്റെ': u'ചക്ക്',
                                         u'അച്ചിന്റെ': u'അച്ച്',
                                         u'പട്ടിന്റെ': u'പട്ട്',
                                         u'കത്തിന്റെ': u'കത്ത്',
                                         u'തേപ്പിന്റെ': u'തേപ്പ്',
                                         u'പയ്യിന്റെ': u'പയ്യ്',
                                         u'കല്ലിന്റെ': u'കല്ല്',
                                         u'രാമുവിന്റെ': u'രാമു',
                                         u'കാടിന്റെ': u'കാട്',
                                         })
        for expected, word in words.items():
            obtained = self.vibhakthi_generator.generate(word, 6)
            assert obtained == expected

    def test_locative(self):
        '''
        Locative - ആധാരിക
        '''
        words = collections.OrderedDict({u'അവനിൽ': u'അവൻ',
                                         u'അവളിൽ': u'അവൾ',
                                         u'രമയിൽ': u'രമ',
                                         u'കഴിവിൽ': u'കഴിവ്',
                                         u'ആപത്തിൽ': u'ആപത്ത്',
                                         u'വാകയിൽ': u'വാക',
                                         u'അനിലിൽ': u'അനിൽ',
                                         u'അവരിൽ': u'അവർ',
                                         u'ചെമ്മണ്ണിൽ': u'ചെമ്മണ്ണ്',
                                         u'വാത്മീകത്തിൽ': u'വാത്മീകം',
                                         u'ചക്കിൽ': u'ചക്ക്',
                                         u'അച്ചിൽ': u'അച്ച്',
                                         u'പട്ടിൽ': u'പട്ട്',
                                         u'കത്തിൽ': u'കത്ത്',
                                         u'തേപ്പിൽ': u'തേപ്പ്',
                                         u'പയ്യിൽ': u'പയ്യ്',
                                         u'കല്ലിൽ': u'കല്ല്',
                                         u'രാമുവിൽ': u'രാമു',
                                         u'കാട്ടിൽ': u'കാട്'
                                         })
        for expected, word in words.items():
            obtained = self.vibhakthi_generator.generate(word, 7)
            assert obtained == expected
