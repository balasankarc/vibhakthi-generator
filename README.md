# VibhakthiGenerator
VibhakthiGenerator is a library to generate different Vibhakthi forms of a word.

## Usage
```
>>> from libindic.VibhakthiGenerator import Malayalam as VibhakthiGenerator
>>> vibhakthi_generator = VibhakthiGenerator()
>>> result = vibhakthi_generator.generate('രാമൻ', 3)
>>> print result
രാമനോട്
```

### Vibhakthi map
 1. നിർദ്ദേശിക (Nominative)
 2. പ്രതിഗ്രാഹിക (Accusative)
 3. സംയോജിക ( Conjuctive)
 4. ഉദ്ദേശിക (Dative)
 5. പ്രയോജിക (Instrumental)
 6. സംബന്ധിക (Genitive / Possessive)
 7. ആധാരിക (Locative)

## Major Problems
Rule based Vibhakthi generator will fail on words that end with those letters whose base forms are ambiguous.Example, the words ending with ർ whose base form can be either ര or റ. Different words ending with ർ, that have similar structure has different results when applying the same vibhakthi.

Example :
* അനിവർ + സംബന്ധിക = അനിവറിന്റെ
* മലർ + സംബന്ധിക = മലരിന്റെ
* കൗരവർ + സംബന്ധിക = കൗരവരുടെ

Check #1 for discussions.

## Testing
```
$ sudo apt-get install python-testtools
$ python -m testtools.run
```
