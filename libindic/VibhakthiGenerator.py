#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Vibhakthi Generator library for Indic languages.
'''

# Copyright: 2016 Balasankar C <balasankarc@autistici.org>
# License: GPL-3.0+

from libindic.syllabifier import Syllabalizer


class Malayalam(object):
    '''
    Vibhakthi Generator library for Malayalam.
    '''

    def __init__(self):
        '''
        Initialize rules.
        '''
        self.exceptions = {u'\u0d1f\u0d4d': [u'',
                                             u'\u0d1f\u0d3f\u0d28\u0d46',
                                             u'\u0d1f\u0d3f\u0d28\u0d4b\u0d1f\u0d4d',
                                             u'\u0d1f\u0d3f\u0d28\u0d4d',
                                             u'\u0d1f\u0d3f\u0d28\u0d3e\u0d7d',
                                             u'\u0d1f\u0d3f\u0d28\u0d4d\u0d31\u0d46',
                                             u'\u0d1f\u0d4d\u0d1f\u0d3f\u0d7d'
                                             ]
                           }
        self.consonants = [u'\u0d15',
                           u'\u0d16',
                           u'\u0d17',
                           u'\u0d18',
                           u'\u0d19',
                           u'\u0d1a',
                           u'\u0d1b',
                           u'\u0d1c',
                           u'\u0d1d',
                           u'\u0d1e',
                           u'\u0d1f',
                           u'\u0d20',
                           u'\u0d21',
                           u'\u0d22',
                           u'\u0d23',
                           u'\u0d24',
                           u'\u0d25',
                           u'\u0d26',
                           u'\u0d27',
                           u'\u0d28',
                           u'\u0d2a',
                           u'\u0d2b',
                           u'\u0d2c',
                           u'\u0d2d',
                           u'\u0d2e',
                           u'\u0d2f',
                           u'\u0d30',
                           u'\u0d32',
                           u'\u0d35',
                           u'\u0d36',
                           u'\u0d37',
                           u'\u0d38',
                           u'\u0d39',
                           u'\u0d33',
                           u'\u0d34',
                           u'\u0d31',
                           u'\u0d3f',
                           u'\u0d4b']
        self.rules = {u'consonants': [u'',
                                      u'\u0d2f\u0d46',
                                      u'\u0d2f\u0d4b\u0d1f\u0d4d',
                                      u'\u0d2f\u0d4d\u0d15\u0d4d\u0d15\u0d4d',
                                      u'\u0d2f\u0d3e\u0d7d',
                                      u'\u0d2f\u0d41\u0d1f\u0d46',
                                      u'\u0d2f\u0d3f\u0d7d'],
                      u'\u0d02': [u'',
                                  u'\u0d24\u0d4d\u0d24\u0d3f\u0d28\u0d46',
                                  u'\u0d24\u0d4d\u0d24\u0d3f\u0d28\u0d4b\u0d1f\u0d4d',
                                  u'\u0d24\u0d4d\u0d24\u0d3f\u0d28\u0d4d',
                                  u'\u0d24\u0d4d\u0d24\u0d3f\u0d28\u0d3e\u0d7d',
                                  u'\u0d24\u0d4d\u0d24\u0d3f\u0d28\u0d4d\u0d31\u0d46',
                                  u'\u0d24\u0d4d\u0d24\u0d3f\u0d7d'],
                      u'\u0d41': [u'',
                                  u'\u0d41\u0d35\u0d3f\u0d28\u0d46',
                                  u'\u0d41\u0d35\u0d3f\u0d28\u0d4b\u0d1f\u0d4d',
                                  u'\u0d41\u0d35\u0d3f\u0d28\u0d4d',
                                  u'\u0d41\u0d35\u0d3f\u0d28\u0d3e\u0d7d',
                                  u'\u0d41\u0d35\u0d3f\u0d28\u0d4d\u0d31\u0d46',
                                  u'\u0d41\u0d35\u0d3f\u0d7d'],
                      u'\u0d4d': [u'',
                                  u'\u0d3f\u0d28\u0d46',
                                  u'\u0d3f\u0d28\u0d4b\u0d1f\u0d4d',
                                  u'\u0d3f\u0d28\u0d4d',
                                  u'\u0d3f\u0d28\u0d3e\u0d7d',
                                  u'\u0d3f\u0d28\u0d4d\u0d31\u0d46',
                                  u'\u0d3f\u0d7d'],
                      u'\u0d7a': [u'',
                                  u'\u0d23\u0d3f\u0d28\u0d46',
                                  u'\u0d23\u0d3f\u0d28\u0d4b\u0d1f\u0d4d',
                                  u'\u0d23\u0d3f\u0d28\u0d4d',
                                  u'\u0d23\u0d3f\u0d28\u0d3e\u0d7d',
                                  u'\u0d23\u0d3f\u0d28\u0d4d\u0d31\u0d46',
                                  u'\u0d23\u0d3f\u0d7d'],
                      u'\u0d7b': [u'',
                                  u'\u0d28\u0d46',
                                  u'\u0d28\u0d4b\u0d1f\u0d4d',
                                  u'\u0d28\u0d4d',
                                  u'\u0d28\u0d3e\u0d7d',
                                  u'\u0d28\u0d4d\u0d31\u0d46',
                                  u'\u0d28\u0d3f\u0d7d'],
                      u'\u0d7c': [u'',
                                  u'\u0d30\u0d46',
                                  u'\u0d30\u0d4b\u0d1f\u0d4d',
                                  u'\u0d7c\u0d15\u0d4d\u0d15\u0d4d',
                                  u'\u0d30\u0d3e\u0d7d',
                                  u'\u0d30\u0d41\u0d1f\u0d46',
                                  u'\u0d30\u0d3f\u0d7d'],
                      u'\u0d7d': [u'',
                                  u'\u0d32\u0d3f\u0d28\u0d46',
                                  u'\u0d32\u0d3f\u0d28\u0d4b\u0d1f\u0d4d',
                                  u'\u0d32\u0d3f\u0d28\u0d4d',
                                  u'\u0d32\u0d3f\u0d28\u0d3e\u0d7d',
                                  u'\u0d32\u0d3f\u0d28\u0d4d\u0d31\u0d46',
                                  u'\u0d32\u0d3f\u0d7d'],
                      u'\u0d7e': [u'',
                                  u'\u0d33\u0d46',
                                  u'\u0d33\u0d4b\u0d1f\u0d4d',
                                  u'\u0d7e\u0d15\u0d4d\u0d15\u0d4d',
                                  u'\u0d33\u0d3e\u0d7d',
                                  u'\u0d33\u0d41\u0d1f\u0d46',
                                  u'\u0d33\u0d3f\u0d7d']}

        self.syllabalizer = Syllabalizer()

    def generate(self, word, vibhakthi):
        '''
        Returns inflected form of word, obtained when applying the
        specified vibhakthi.
        '''
        result = None
        word = word.strip()
        vibhakthi = int(vibhakthi) - 1
        try:
            root_word = word.decode('utf-8')
        except UnicodeEncodeError:
            root_word = word
        if vibhakthi == 0:
            return root_word
        tokens = self.syllabalizer.syllabify_ml(root_word)
        prefix = u''
        for token in tokens[:-1]:
            prefix += token
        last_token = tokens[-1]
        if last_token in self.exceptions:
            suffix = self.exceptions[last_token][vibhakthi]
            result = prefix + suffix
            return result
        else:
            count = 1
            while count <= len(last_token):
                last_char = last_token[-count:]
                if last_char in self.rules:
                    prefix = prefix + last_token[:len(last_token) - count]
                    suffix = self.rules[last_char][vibhakthi]
                    result = prefix + suffix
                    return result
                elif last_char in self.consonants:
                    result = root_word + self.rules[u'consonants'][vibhakthi]
                    return result
                count = count + 1
