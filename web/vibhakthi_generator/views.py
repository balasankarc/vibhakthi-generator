import sys
import os
sys.path.insert(0, os.path.abspath(os.path.join(__file__, '../../../')))
from libindic.VibhakthiGenerator import Malayalam as VibhakthiGenerator

from flask import request, render_template
import json

from vibhakthi_generator import app


@app.route("/")
def home():
    return render_template('home.html')


@app.route("/api/inflect", methods=['GET', 'POST'])
def inflect():
    word = request.form['word'].strip()
    vibhakthi = request.form['vibhakthi'].strip()
    result = None
    if word and vibhakthi:
        if vibhakthi == '0':
            result = "Select Vibhakthi"
        else:
            vibhakthi_generator = VibhakthiGenerator()
            result = vibhakthi_generator.generate(word, int(vibhakthi))
            if not result:
                result = "Error"
        output = {}
        output[word] = result
        return json.dumps(output)
    else:
        return word
